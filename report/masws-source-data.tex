\documentclass[a4paper,onecolumn,oneside]{article}

\usepackage{hyperref}
\usepackage{parskip}
\usepackage{graphicx}

\title{Multi-agent Semantic Web Systems\\Assignment 1}
\author{Clemens Wolff (s0942284)}
\date{February 7, 2014}

\begin{document}
\maketitle

\section{Introduction}\label{sec:introduction}

This report introduces the BoardGameGeek data-set, argues for the benefits of
making the data available in the Resource Description Framework (RDF) format and
explores some of the technicalities involved in doing so.

The report is structured as follows.  The BoardGameGeek data-set is described in
Section~\ref{sec:data}.  Section~\ref{sec:queries} argues a case for making
the data-set available in RDF by giving sample queries that might be interesting
to run on the data-set.  Various methods to convert the data into RDF are
explored in Section~\ref{sec:conversion}.  Section~\ref{sec:challenges} gives a
brief nod to potential challenges that might occur during the implementation of
the work outlined in Sections~\ref{sec:queries}~and~\ref{sec:conversion}.
Finally, Section~\ref{sec:conclusion} synthesises the findings and concludes the
report.

\section{The BoardGameGeek data-set}\label{sec:data}

The BoardGameGeek data-set is a collection of information about board-games and
board-game hobbyists aggregated from \url{BoardGameGeek.com} (BGG). BGG is a
popular \cite{alexa-page-rank} and critically acclaimed \cite{dianajones-award}
web-site dedicated to board-games. The site combines a comprehensively curated
database of board-games with an active community augmenting the board-games
catalogue via crowd-sourced information.

The database of BGG stores factoids about more than 60,000 board-games (e.g.\
author, publisher, release date, available expansions, awards won, etc.).
Furthermore, publisher-released details about games are also aggregated by the
BGG database: game description, components, suggested number of players,
approximate playing time, recommended player age, etc.

The BGG community augments these factoids by providing game-ratings, reviews and
crowd-sourced validation of various publisher-released details.  For instance,
users can vote on how many players the game is best with, or on the complexity
or language-dependency of a game.

A sample overview of the data available on BGG is given by
Figure~\ref{fig:bgg-game-info}.  All the information stored in the BGG database
is made available to the public via a simple XML API documented in
\cite{bgg-xmlapi}.  The BoardGameGeek data-set is the result of an exhaustive
crawl of this API, containing information about more than 138,000 boardgames and
more than 38,000 BGG users.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{bgg-game-info.png}
\caption{Sample overview of the curated and community-based data available on
BGG for the game ``Takenoko'' \cite{bgg-takenoko-main,bgg-takenoko-xml}}
\label{fig:bgg-game-info}
\end{figure}

\section{Querying the data}\label{sec:queries}

Making the BGG data available in RDF will enable queries answering questions
such as:
\begin{itemize}
\item Does the publisher-provided information about games (e.g.\ number of
players, age, etc.) correlate with the experiences of the BGG community?
\item Is there evidence for the ``cult of the new'' i.e.\ are new games rated
higher than old games?
\item Which (if any) authors or publishers are ``magnets'' for awards?
\item To what extent do BGG game-ratings agree with industry awards?
\item Which users in the BGG community are most similar?
\end{itemize}

Another main motivating factor for making the BGG data available in an easy to
process format such as RDF is that this will enable development of better
board-game recommendation systems --- an area lacking well engineered
contenders.  Existing board-game recommendation systems are either defunct or
abandoned (e.g.\ \cite{bgg-recs}) or focus too much on commercially successful
games (e.g.\ \cite{amazon-recs}).

Combining the power of the RDF graph and the large amount of data available on
BGG will enable filling this niche.  A simple recommendation system could, for
example, use the structure of the RDF graph to analyse how games connect to
other games (via BGG users).  This will expose clusters of games often enjoyed
together.

\section{Converting the data to RDF}\label{sec:conversion}

The data on BGG is made accessible via an XML API\@.  Like a lot of data on the
Internet, the data on BGG does does not conform to any particular ontology but
rather uses its own schema to present the data.  There are thus two steps
necessary in order to convert the BGG data to RDF: find suitable ontologies for
the domain and map those ontologies onto the data while converting the XML to
RDF.

The task of finding an ontology appropriate for the BoardGameGeek data-set can
be broken down into three sub-tasks:
\begin{enumerate}
\item Finding an ontology to represent the ``rates''-relationship between
board-games and individual users in the BGG community.
\item Finding an ontology to express the factual
and community-sourced information about board-games.
\item Finding an ontology to represent the BGG community.
\end{enumerate}

Several ontologies exist for the first task. For example, the ``review
ontology'' \cite{review-ontology} captures discrete-interval ratings of
arbitrary data.  The more refined ``ratings ontology'' \cite{ratings-ontology}
allows for ratings of arbitrary precision.

The second task is less well served by ontology vocabularies.  However, factual
information about the board-games can be expressed using the board-game
vocabulary \cite{games-ontology}.  Data about board-game awards can be expressed
using the ``co-creation event'' vocabulary \cite{awards-ontology}.

The third task can be solved by using a subset of the ``semantically-interlinked
on-line communities'' vocabulary \cite{sioc-ontology}.

After finding suitable ontologies to model the domain of the BoardGameGeek
data-set, the task of converting the data to RDF is a simple question of
engineering.  A multitude of tools exists to convert XML to RDF\@.  As far as
this report is concerned, the main engineering consideration for the task is
trading-off using an all-in-one toolbox such as Apache Jena for Java \cite{jena}
versus having to combine multiple libraries but being able to use a more
productive programming language such as Python.  In the later case, xml2rdf
\cite{xml2rdf} or hand-written XPath queries could be used to convert the XML to
RDF and libRDF \cite{librdf} or RDFlib \cite{rdflib} could be used as a
front-end to the resulting RDF data representation.

\section{Potential difficulties}\label{sec:challenges}

One potential challenge for the conversion of the BoardGameGeek data-set to RDF
was introduced in Section~\ref{sec:data}: the BoardGameGeek data-set is quite
comprehensive.  Having to handle about 4GB of data might make implementation
more challenging and time consuming.

Section~\ref{sec:conversion} introduced another potential difficulty: the
BoardGameGeek data-set covers many different domains (board-games, ratings,
awards, users, etc.).  This means that many different ontologies have to be
found and combined in order to describe the data in the BoardGameGeek data-set,
thus increasing complexity of the to-RDF conversion task.

On a more minor note, some queries introduced in Section~\ref{sec:queries} might
be time-consuming to run in RDF because of the need of arithmetic and
aggregation.

\section{Conclusion}\label{sec:conclusion}

This report introduced the BoardGameGeek data-set that combines factual
information about board-games with data gathered from a large on-line community
centred around board-games (Section~\ref{sec:data}).  Section~\ref{sec:queries}
argued for the importance of converting the data-set into RDF format by
providing sample queries that might be of interest to board-game enthusiast.
Technical details of how this conversion could be made and potential
implementation challenges were discussed in
Sections~\ref{sec:conversion}~and~\ref{sec:challenges}.

\begin{thebibliography}{32}

\bibitem{alexa-page-rank}
    Alexa Internet Inc.,
    \emph{How popular is boardgamegeek.com?},
    \url{http://www.alexa.com/siteinfo/boardgamegeek.com},
    retrieved 7 February 2014.

\bibitem{dianajones-award}
    The Diana Jones Award for Excellence in Gaming,
    \emph{The Diana Jones Award 2010},
    \url{http://www.dianajonesaward.org/10winner.html},
    retrieved 7 February 2014.

\bibitem{bgg-xmlapi}
    BoardGameGeek.com,
    \emph{BGG XML API},
    \url{http://boardgamegeek.com/wiki/page/BGG_XML_API},
    retrieved 7 February 2014.

\bibitem{bgg-takenoko-main}
    BoardGameGeek.com,
    \emph{Takenoko},
    \url{http://www.boardgamegeek.com/boardgame/70919},
    retrieved 7 February 2014.

\bibitem{bgg-takenoko-xml}
    BoardGameGeek.com XML API,
    \emph{Takenoko},
    \url{http://www.boardgamegeek.com/xmlapi/boardgame/70919},
    retrieved 7 February 2014.

\bibitem{bgg-recs}
    BoardGameGeek.com,
    \emph{Personal Recommendations},
    \url{http://boardgamegeek.com/wiki/page/Personal_Recommendations},
    retrieved 7 February 2014.

\bibitem{amazon-recs}
    Amazon.com Inc.,
    \emph{Board Games Store},
    \url{http://goo.gl/O2k1pJ},
    retrieved 7 February 2014.

\bibitem{review-ontology}
    RDF Review Vocabulary,
    \emph{http://vocab.org/review/terms.html},
    retrieved 7 February 2014.

\bibitem{ratings-ontology}
    Longo, Cristiano, and Lorenzo Sciuto,
    \emph{A Lightweight Ontology for Rating Assessments},
    SWAP,
    2007.

\bibitem{games-ontology}
    Epimorphics Ltd.,
    \emph{Ontology for Games},
    \url{http://epimorphics.com/public/vocabulary/games.ttl},
    retrieved 7 February 2014.

\bibitem{awards-ontology}
    Dimou, Anastasia, and Colpaert Pieter,
    \emph{The vocabulary for Co-creation Events based on Open Data},
    \url{http://semweb.mmlab.be/ns/apps4X},
    retrieved 7 February 2014.

\bibitem{sioc-ontology}
    W3C members,
    \emph{Semantically-Interlinked Online Communities (SIOC) Ontology Submission
    Request to W3C},
    \url{http://www.w3.org/Submission/2007/02},
    retrieved 7 February 2014.

\bibitem{xml2rdf}
    German Astronomy Community Grid,
    \emph{A Transformation from XML to RDF via XSLT},
    \url{http://www.gac-grid.org/project-products/Software/XML2RDF.html},
    retrieved 7 February 2014.

\bibitem{jena}
    Apache Software Foundation,
    \emph{Apache Jena},
    \url{http://jena.apache.org},
    retrieved 7 February 2014.

\bibitem{librdf}
    Dave Beckett,
    \emph{Redland RDF Libraries},
    \url{http://librdf.org},
    retrieved 7 February 2014.

\bibitem{rdflib}
    RDFLib Team,
    \emph{RDFlib documentation},
    \url{https://rdflib.readthedocs.org},
    retrieved 7 February 2014.

\end{thebibliography}

\end{document}
