\documentclass[a4paper,onecolumn,oneside]{article}

\usepackage{hyperref}
\usepackage{parskip}
\usepackage{graphicx}
\usepackage{subfigure}

\title{Multi-agent Semantic Web Systems\\Assignment 1 --- Part 2}
\author{Clemens Wolff (s0942284)}
\date{March 5, 2014}

\begin{document}
\maketitle

\section{Introduction}\label{sec:introduction}

This report explores methods to convert the BoardGameGeek (BGG) data-set into
the Resource Description Framework (RDF) format.  Converting a data-set into RDF
entails two main activities.  First, suitable ontologies to model the semantics
of the data have to be selected.  Then, the data has to be converted to fit the
ontologies.  The two main sections of this report are dedicated to these two
tasks.  Section~\ref{sec:ontologies} presents the RDF ontologies used to model
the semantics of the BoardGameGeek data-set.  Engineering considerations arising
when converting the data to RDF are discussed in Section~\ref{sec:conversion}.
Section~\ref{sec:conclusion} synthesises the findings and concludes the report.

\section{Ontologies for the BoardGameGeek Data-Set}\label{sec:ontologies}

This section introduces and justifies the ontologies used to represent the BGG
data-set in RDF.

Recall that the BGG data-set contains information about:
\begin{itemize}
\item Board games: publisher, designer, release date, suggested play time,
suggested number of players, etc.
\item Users of the BGG website.
\item Relationships between users and board games: users comment and rate games.
\end{itemize}
For full detail on the BoadGameGeek data-set, please refer to the last report.

The following ontologies are used to model the data-set:
\begin{itemize}
\item The ``Board game ontology'' \cite{boardgame-ontology} is used to model the
factual information about the board games in the BGG data-set.
\item The ``Friend-of-a-Friend ontology'' \cite{foaf-ontology} (FOAF) is used to
model the users of the BGG website.
\item The ``Ratings ontology'' \cite{ratings-ontology} is used to model the
relationships between uses and board games.
\end{itemize}

Figures~\ref{fig:boardgame-ontology}--\ref{fig:ratings-ontology} show the main
classes (TitleCase) and relationships (camelCase) of the Board game, FOAF and
Ratings ontologies that are used to model the BGG data-set.  The semantics of
the classes and relationships are detailed in Table~\ref{tbl:ontologies}.

The ``Board game ontology'' provides a rudimentary vocabulary to model board
games, board game publishers and a limited set of attributes and relations
thereof (number of players, designer-of, publication date, publisher-of).  The
vocabulary is not sufficient to cover all of the detailed information in the BGG
data-set.  However, the most vital properties of the data-set can be covered
with the board game ontology.  The vocabulary is sufficiently detailed to
capture the bare essence of what a board game is: tagging some Uniform Resource
Identifier (URI) as a board game and providing basic identifying information
about the game.  This report therefore argues that the amount of detail in the
BGG data-set is excessive and that the subset that can be described by the board
game ontology is sufficient to model board games on a fundamental level.  The
resulting loss in data-set descriptiveness is not optimal.  However, the
benefits of using an existing ontology to model the data-set is arguably more
important than being able to capture every nitty-gritty detail about the domain
at hand.  URIs for board games are provided directly by the BGG
website.\footnote{For example, the URI
\url{http://www.boardgamegeek.com/boardgame/66362} identifies the board game
``Glen More'' designed by Matthias Cramer.}

While the ``Board game ontology'' was under-descriptive of the BGG data-set, the
FOAF ontology used to model the users of the BGG website is over-descriptive.
Only a very limited subset of the FOAF vocabulary is used to model the
information about the users of the users of the BGG website in the data-set.
The ``Person'' class is used to indicate that some URI is a user of the BGG
website.  The ``name'' property is used to attach human readable names to
different URIs: users, board games, publishers, designers, etc.  It is not
surprising that only a limited subset of the FOAF ontology is necessary to model
the BGG community.  FOAF is a vocabulary designed to model on-line communities
with rich human interactions.  BGG, on the other hand, is a community where most
interaction happens between humans and board games, not humans and other humans.
Nevertheless, this report argues that FOAF is a good choice to model the
information about the BGG users in the BGG data-set because FOAF is a standard
ontology for the task (and therefore its semantics are likely to be understood
deeply by agents encountering it).  URIs for users of the BGG website are, once
again, provided directly by the website.\footnote{For example, the URI
\url{http://www.boardgamegeek.com/user/spodwolff} identifies the author of this
document as a user of the BGG website.}

The main interaction between users of the BGG website and the board-games in the
BGG data-set is that users rate games.  The ``Ratings ontology'' is thus a great
fit to model the relations between the users of the BGG website and board games.
Close to the entire vocabulary of the Ratings ontology was used to model user
generated ratings of board games.  No information in the original BGG data-set
had to be left out.  The ratings ontology defines a rating as something that is
about a rated resource, was done by an agent and that may have a value.
Additional information on the rating collection process (e.g.\ rating scale and
by whom the ratings were collected) is also captured.  The only minor detail in
which the ratings ontology is not directly and fully applicable to the BGG
data-set stems from the fact that the ratings ontology contains an explicit
``Rating'' class.  The explicit ``Rating'' class stems from the fact that the
Ratings ontology was originally developed to aggregate movie reviews from
different sources.  Being able to identify a URI as being a ``Rating'' is
therefore useful  (e.g.\ to link a rating back to the URL of the review that led
to the rating).  The BGG data-set, on the other hand, does not provide URIs for
ratings.  However, all the non-URI information that makes up a ``Rating'' exists
in the BGG data-set.  The lack of ``Rating''--URIs can therefore can trivially
be solved by using anonymous nodes to represent the ratings.

This section introduced the ontologies used to represent the information in the
BGG data-set.  The BGG data-set mainly consists of information about board
games, users of the BGG website and rating-relationships between users and
games.  These three main components of the BGG data-set are well represented by
the Board game, FOAF and Ratings ontologies.  The resulting RDF graph is
visualised in Figure~\ref{fig:graph}.

\begin{table}
\centering
\begin{tabular}{| l | l | p{6cm} |}
\hline
Word & Type & Semantic interpretation \\
\hline
\hline
BoardGame & Class & Anything that could be considered a board game. \\
\hline
players & BoardGame $\rightarrow$ int & Specifies the number of players of a
board game.  Note that if a board game supports a range of player numbers,
multiple instances of this property need to be specified. \\
\hline
Publisher & Class & Publisher of a board game. \\
\hline
publisher & Publisher $\rightarrow$ BoardGame & Specifies that a publisher
published some specific game. \\
\hline
designed-by & BoardGame $\rightarrow$ Person & Specifies the inventor of a board
game. \\
\hline
pubDate & BoardGame $\rightarrow$ int & Specifies the publication date of a
board game. \\
\hline
\hline
Person & Class & A user of the BGG website. \\
\hline
name & Thing $\rightarrow$ string & Attach a name to a board game, publisher,
designer, \ldots \\
\hline
\hline
Rating & Class & Anything that has exactly one about, assessedBy and collectedBy
properties and at most one value property. \\
\hline
about & Rating $\rightarrow$ BoardGame & The rated board game. \\
\hline
assessedBy & Rating $\rightarrow$ Person & The user rating the board game. \\
\hline
value & Rating $\rightarrow$ int & The rating given by the user to the game. \\
\hline
collectedBy & Rating $\rightarrow$ RatingsCollector & Specification of the
rating scale and where the rating was collected. \\
\hline
\hline
\end{tabular}
\caption{Semantic interpretations of the ontological vocabulary used in the
context of the RDF version of the BGG data-set.}
\label{tbl:ontologies}
\end{table}

\begin{figure}
\begin{subfigure}
\centering
\includegraphics[width=10cm]{boardgame-ontology.pdf}
\caption{Subset of the Boardgame ontology \cite{boardgame-ontology} used to
model the BGG data-set.}
\label{fig:boardgame-ontology}
\end{subfigure}
\begin{subfigure}
\centering
\includegraphics[width=8cm]{foaf-ontology.pdf}
\caption{Subset of the Friend-of-a-Friend ontology \cite{foaf-ontology} used to
model the BGG data-set.}
\label{fig:foaf-ontology}
\end{subfigure}
\begin{subfigure}
\centering
\includegraphics[width=12cm]{ratings-ontology.pdf}
\caption{Subset of the Ratings ontology \cite{ratings-ontology} used to model
the BGG data-set.  Dashed contours indicate anonymous nodes.}
\label{fig:ratings-ontology}
\end{subfigure}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{graph.pdf}
\caption{RDF graph for the BGG data-set.  Ovals are classes, edges are
properties, rectangles are literals. ``egc'', ``foaf'' and ``rat'' are prefixes
for the Board game, Friend-of a-Friend and Ratings ontologies respectively.}
\label{fig:graph}
\end{figure}

\section{Engineering RDF Converters}\label{sec:conversion}

This section discusses how the BGG data-set is mapped onto the ontologies
presented in Section~\ref{sec:ontologies}.

Recall that the BGG data-set is quite large (it exceeds 4GB in size).  Due to
time and computing resource constraints, the rest of this section only considers
a small subset (about 1\%) for conversion to RDF\@.  Note, however, that all
conversion methods have also been tested on bigger input sizes.  This report is
confident that given enough time and computing resources, it would be trivial to
convert the entire data-set to RDF.

The Notation3 (N3) \cite{notation3} file format is used to store the BGG
data-set after it has been converted to RDF\@.  This file format was chosen
because it is very human readable and easy to write programmatically.  In order
to further increase simplicity of the RDF representation and composability of
the data-creation process, every piece of information in the BGG data-set is
represented in one explicit triple (i.e.\ ``;'' is never used to concatenate
statements).  This means that every piece of information can be extracted from
the BGG data-set in isolation from the remaining data-extraction process.
Instead of extracting all the information at the same time, the information
extraction process can be decomposed into isolated components that can be
re-combined at a later time.  This means that very simple methods can be used to
populate the RDF graph representing the BGG data-set.

The RDF graph of Figure~\ref{fig:graph} was populated using Python scripts that
run XPath queries on the XML structure of the BGG data-set.\footnote{Recall that
the last report mentioned the possibility of hand-rolling a to-RDF converter as
a learning exercise.}  The back-end executing the XPath queries is LXML
\cite{lxml}.  The thus-generated N3 RDF files are validated using a custom
syntax checker based on the Apache Jena library \cite{jena}.

Populating the RDF graph using XPath queries worked very well.  The only
engineering hick-up faced was that LXML does not support XPath queries that aim
to retrieve node attributes.\footnote{A sample query illustrating this is
\url{.//boardgame/comment[@rating]/@username}.  The query aims to extract the
names of all users that have rated a given game.  ``username'' and ``rating''
are an attributes of the comment node.}  This means that some of the code used
to populate the RDF graph is less general and re-usable than desired.  Another
minor engineering difficulty faced is the lack of support for the N3 file format
in a lot of RDF processing tools.  Many tools expect RDF graphs to be serialised
as RDF/XML \cite{rdfxml}.  In order to be able to use these tools, the {\tt
rdfcat} utility included with Apache Jena had to be used to convert the N3
format to RDF/XML.

This section presented the process that was used to convert the BGG data-set to
RDF\@. XPath queries were used to populate the RDF graph; the N3 file format was
used to serialise the graph.  Engineering challenges faced included data-set
size (solved by considering only a subset of the data) and incomplete XPath
support in the LXML Python library (solved by writing less general, more
domain-specific code).

\section{Conclusion}\label{sec:conclusion}

This report described a process to convert the BoardGameGeek data-set into
RDF\@.  The task involved two main sub-components: choosing suitable ontologies
to represent the domain and mapping the data-set onto those ontologies.  The
ontologies used to represent the domain of the data-set were presented in
Section~\ref{sec:ontologies}.  Section~\ref{sec:conversion} introduced some of
the engineering decisions and challenges faced during the data-mapping process.

The RDF version of the BGG data-set is available here:\newline
\url{http://homepages.inf.ed.ac.uk/s0942284/masws/bgg.n3}

\begin{thebibliography}{32}

\bibitem{boardgame-ontology}
    Epimorphics Ltd.,
    \emph{Ontology for Games},
    \url{http://epimorphics.com/public/vocabulary/games.ttl},
    retrieved 5 March 2014.

\bibitem{foaf-ontology}
    Miller, Libby and Brickley, Dan,
    \emph{Friend of a Friend Ontology},
    \url{http://xmlns.com/foaf/spec/#},
    retrieved 5 March 2014.

\bibitem{ratings-ontology}
    Longo, Cristiano and Lorenzo Sciuto,
    \emph{A Lightweight Ontology for Rating Assessments},
    \url{http://www.tvblob.com/ratings/#},
    SWAP,
    2007.

\bibitem{notation3}
    Berners-Lee, Tim and Connolly, Dan,
    \emph{Notation3 (N3): A readable RDF syntax},
    \url{http://www.w3.org/TeamSubmission/n3/},
    retrieved 5 March 2014.

\bibitem{lxml}
    LXML,
    \emph{lxml - XML and HTML with Python},
    \url{http://lxml.de/},
    retrieved 7 February 2014.

\bibitem{jena}
    Apache Software Foundation,
    \emph{Apache Jena},
    \url{http://jena.apache.org},
    retrieved 7 February 2014.

\bibitem{rdfxml}
    McBride, Brian and Beckett, Dave,
    \emph{RDF/XML Syntax Specification (Revised)},
    \url{http://www.w3.org/TR/REC-rdf-syntax/},
    retrieved 7 February 2014.

\end{thebibliography}

\end{document}
