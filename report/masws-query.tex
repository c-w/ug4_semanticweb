\documentclass[a4paper,onecolumn,oneside]{article}

\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{fancyvrb}
\usepackage{parskip}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{caption}
\usepackage[a4paper]{geometry}
\VerbatimFootnotes

\title{Multi-agent Semantic Web Systems\\Assignment 2}
\author{Clemens Wolff (s0942284)}
\date{March 21, 2014}

\begin{document}
\maketitle

\section{Introduction}\label{sec:introduction}

This report explores methods to extract information from the RDF\footnote{RDF
stands for ``Resource Description Framework''.} \cite{rdf} version of the
Board-Game-Geek (BGG) data-set.  Section~\ref{sec:data} gives a brief refresher
on the nature and contents of the BGG data-set.  Section~\ref{sec:queries}
proposes and motivates queries that extract a representative sample of
information from the BGG data-set using SPARQL\footnote{SPARQL stands for
``SPARQL Protocol and RDF Query Language''.} \cite{sparql}.  The benefits of
using SPARQL and RDF to query the BGG data-set over other information extraction
techniques are explored in Section~\ref{sec:benefits}.
Section~\ref{sec:conclusion} synthesises the findings and concludes the report.

\section{The BoardGameGeek Data-Set}\label{sec:data}

The BoardGameGeek data-set is an amalgamation of information about board-games
scraped from the website of the same
name.\footnote{The BGG data-set was scraped from
\url{http://www.boardgamegeek.com}.} The data-set contains factoids about games
(e.g.: Who designed the game? When was it published? etc.) and user-generated
information such as game-ratings (e.g.: On a scale of 1 to 10, how much did user
X like game Y).  The data-set thus captures relations between games and people
(who released the game, who made the game) as well as relations between people
and games (who likes which game).

The BoardGameGeek website provides URIs for board-games, publishers, designers,
users, and so forth.  These URIs are made heavy use of in the RDF version of the
data-set.\footnote{A small subset of the RDF version of the BGG data-set can be
found at \url{http://www.homepages.inf.ed.ac.uk/s0942284/masws/bgg.n3}.}  The
BGG data-set's RDF serialisation furthermore uses the following ontologies to
model the information contained in the data:
\begin{itemize}
\item The games ontology \cite{egc} is used to encode factual information about
board-games.
\item The ratings ontology \cite{rat} is used to encode user-generated game
reviews.
\item The friend-of-a-friend ontology \cite{foaf} is used to map between.
\end{itemize}

The graph on page~\pageref{fig:graph} summarises the BGG data-set's RDF schema.
For a comprehensive overview of the data-set, please refer to the two past
reports in this series.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{graph.pdf}
\caption*{RDF graph for the BGG data-set.  Ovals represent classes, edges
represent properties, rectangles represent literals. ``egc'', ``foaf'' and
``rat'' are prefixes for the Board game, Friend-of a-Friend and Ratings
ontologies respectively.}
\label{fig:graph}
\end{figure}

\section{Querying the RDF Graph}\label{sec:queries}

This section is concerned with extracting information from the RDF serialisation
of the BGG data-set.  The information extraction process is backed by the {\tt
arq} binary included with Apache Jena \cite{jena}.  {\tt arq} is a tool that
allows its client to run SPARQL queries against the RDF graph of a data-set.
SPARQL is query language for linked databases that allows a client to perform
triple-matching and pattern-matching over a data-set's RDF graph.

Some of the questions that this section aims to answer were mentioned in the
first report in this series: Are new games better received than old games?  Can
the BGG data-set be used as a back-bone for board-game recommendations?  The
following sub-sections also cover questions that demonstrate the value gained
from representing the BGG data-set using RDF and querying it using SPARQL rather
than using some other data-store and query language (such as XML\footnote{XML
(read: ``Extensible Markup Language'') is a tree-based data-serialisation format
that is widely used for data-interchange.} and XPath\footnote{XPath (short for
``XML Path Language'') is a language to navigate the tree structure of XML
documents and retrieve selected nodes.} or a RDBMS\footnote{RDBMS stands for
``Relational Database Management System''.} and SQL\footnote{SQL (i.e.\ the
``Structured Query Language'') is a widely used declarative language to extract
information from relational database systems.}).

\subsection{Aggregate game-facts}\label{sec:gameinfo}

The RDF serialisation of the BGG data-set was created by running XPath queries
against the XML-responses scraped from the BGG's data-access
service.\footnote{The BGG's data-access point can be found at
\url{www.boardgamegeek.com/xmlapi/boardgame}.}  The last report in this
series showed that the process of extracting information from the XML structure
in this way was very straight-forward and effective.  It is therefore essential
to demonstrate that SPARQL is able to re-retrieve the information from the RDF
serialisation just as easily.

Figure~\ref{fig:rq-gameinfo} (page~\pageref{fig:rq-gameinfo}) shows a SPARQL
query that retrieves various tidbits of information about each game in the BGG
database.  The results of running the query can be found in
Table~\ref{tbl:rq-gameinfo} (page~\pageref{tbl:rq-gameinfo}).  Retrieving
simple, linked data from a RDF graph is made trivial using SPARQL\@.  Simply
start at some node and follow the node's outgoing or incoming edges.  This will
retrieve relationships between the entities in the database.  The SPARQL code to
do this is as follows:
\begin{itemize}
\item Start at a board-game in the graph: \verb|?game rdf:type egc:BoardGame|,
\item Follow an outgoing edge: \verb|?game egc:designed-by ?designer|,
\item Follow an incoming edge: \verb|?publisher egc:publisher ?game|.
\end{itemize}

Also note the use of the \verb|GROUP_CONCAT| function to make the presentation
of multiple publishers or designers more readable and the resulting lack of a
\verb|DISTINCT| specifier in the \verb|SELECT| clause of the query (grouping
handles duplicate keys by definition).

Arguably, RDF's way of representing relationships in a database is more flexible
and allows for simpler retrieval of information than the tree structure of
XML or the entity-relation structure of a RDBMS\@.  Querying the latter is
coupled to a particular serialisation of the data-set (i.e.\ syntax).  Querying
the former, however, can focus on the semantics implied by the data and is thus
more re-usable (as long as the same ontologies are used to represent data from
the same domain).

\subsection{Aggregate game-ratings}\label{sec:rank}

In order to answer some of the more involved questions mentioned in the
introduction to this section (e.g.\ game recommendation systems based on user
similarities or investigating the evolution of game ratings over time) the
ability to aggregate the numeric values contained in the BGG data-set (and
performing arithmetic on them) is essential.

Figures~\ref{fig:rq-rankpublishers}--\ref{fig:rq-rankgames}
(pages~\pageref{tbl:rq-rankpublishers}--\pageref{tbl:rq-rankgames}) show two
SPARQL queries that aggregate the user-generated rating information in the BGG
data-set.  The results of the queries are in
Tables~\ref{tbl:rq-rankpublishers}--\ref{tbl:rq-rankgames}
(pages~\pageref{tbl:rq-rankpublishers}--\pageref{tbl:rq-rankgames}).  The former
query ranks game publishers by the average score of the games they have
released.  The latter ranks the games of a particular designer by the average
rating the games receive.

Performing aggregate queries in SPARQL is quite similar to how one would perform
the same searches in SQL: aggregation functions (\verb|MAX|, \verb|MIN|,
\verb|AVG|, \verb|COUNT|, etc.) are used to summarise database-entries along
some dimensions specified by the \verb|GROUP BY| key.  \verb|HAVING| can be used
to restrict the result-set along one or more of the aggregate dimension.

Unlike in SQL, non-aggregate result-sets are restricted in SPARQL using the
\verb|FILTER| directive (e.g.\
\verb|FILTER REGEX(?designer_name, "knizia", "i")| can be used to only include
designers whose name field matches ``Knizia'' in the results of the query).

Just like SQL, SPARQL has poor support for floating point rounding.  Note the
use of the \verb|ROUND| function in conjunction with integer
multiplication/division by $10^n$ as a work-around to round a number to $n$
significant places.

Besides being an exercise to prove that using SPARQL does not lead to a loss in
aggregation-query expressiveness (as compared to a different query language such
as SQL), the results in
Tables~\ref{tbl:rq-rankpublishers}--\ref{tbl:rq-rankgames} also have an interest
in their own right.

For instance, Table~\ref{tbl:rq-rankpublishers} confirms the fact that Germany
is the epicentre of the board-gaming sphere: four out of the top ten publishers
are German.\footnote{Hans im Gl\"{u}ck, Ravensburger, Amigo and Abacus.}
However, the table also reveals an additional interesting subtlety.  The
publisher with the very best reviews\footnote{999 Games.} is not German but
Dutch.  The publisher has also released more games and attracted more reviews
than any of the German publishers taken individually.  This is a likely
indicator of a strong (but centralised) gaming culture in the Netherlands.  It
is similarly unexpected that there is a Korean board-game
publisher\footnote{Koreaboardgames.} among the top 10 publishers --- with a
similar games-published to reviews-achieved ratio as any of the major German
publishers.  The query in Figure~\ref{fig:rq-rankpublishers}, simple as it may
be, has thus led to two interesting observations about the state of board-gaming
outside of the German nucleus.

\subsection{Spot trends in game quality over time}\label{sec:rankyears}

Next, one of the questions raised in the first report of this series is
investigated: is there evidence for the cult of the new i.e.\ do now games get
higher review-scores than old games?  The relevance of this question was argued
in the first report in this series and is further justified by the question's
frequently reoccurring nature in the BGG community.\footnote{The search-engine
Google returns more than 28,600 pages when searching for ``cult of the new'' on
the BGG website!}

Figure~\ref{fig:rq-rankyears} (page~\pageref{fig:rq-rankyears}) shows a SPARQL
query to answer the question.  The query should look very familiar after seeing
the queries in the last section: RDF graph traversal is used to find all the
games published in a certain year and an aggregate function is used to find the
average rating of the games.  For easier interpretability, the results are
restricted to only include games published in the nineties.\footnote{Wide-spread
adoption and propagation of board-games didn't really start until the nineties
--- any results before that point will therefore be noisy.  N.B.: even without
any domain knowledge, this statement can easily be verified by running the query
of Figure~\ref{fig:rq-rankyears} without the \verb|FILTER| clause.  The results
show that only from the nineties onwards there are consistently dense numbers of
games published and high numbers of reviewed games per year.}  The results of
the query can be found in Table~\ref{tbl:rq-rankyears}
(page~\pageref{tbl:rq-rankyears}).  The results are also visually displayed in
the plot on page~\pageref{fig:release-rating}.  The findings indicate that,
barring an outlier in 1995,\footnote{The outlier in 1995 is due to a number of
hugely popular and influential games being released that year (e.g.\ ``El
Grande'' or ``The Settlers of Catan'').} there does seem to be a slow but steady
trend of newer games getting better reviews.

\begin{figure*}
\centering
\includegraphics[width=\textwidth]{release-rating.png}
\caption*{Plot of the average game-review-score by year}
\label{fig:release-rating}
\end{figure*}

\subsection{Simple board-game recommendations}\label{sec:similargames}

Using slightly more advanced SPARQL queries, simple board-game recommendation
systems can be built, thus solving another one of the questions raised in the
first part of this report series.\footnote{The need for board-game
recommendation systems was justified in the first report in this series: there
currently simply are no good engines available meaning that any progress in the
field is a boon.}

Figures~\ref{fig:rq-similargames}--\ref{fig:rq-similarusers}
(pages~\pageref{fig:rq-similargames}--\pageref{fig:rq-similarusers}) show two
SPARQL queries realising two simple board-game recommendation systems.  The
results of the queries can be found in
Tables~\ref{tbl:rq-similargames}--\ref{tbl:rq-similarusers}
(pages~\pageref{tbl:rq-similargames}--\pageref{tbl:rq-similarusers}).

The first recommendation system is quite simplistic.  Say a user $U$ has found
out that he likes games by some designer $D$.  The recommendation system
retrieves all games by $D$ (using a similar combination of graph-traversal plus
filtering as discussed for the query in Figure~\ref{fig:rq-rankgames}) and
removes any games that $U$ has already rated using \verb|MINUS|.\footnote{The
\verb|MINUS| operand computes the difference between two sets.}  Thus, the
recommendation system presents to $U$ all games designed by $D$ unknown to $U$.

The second recommendation system is slightly more involved.  Given a user $U_1$,
the system finds the user $U_2$ who has the highest number of games in common
with $U_1$.  The system then recommends any games that $U_2$ rated and $U_1$ has
not rated.  The system is implemented in SPARQL, using a sub-query to find
$U_2$.  The result of the sub-query is joined with the set of games rated by
$U_2$ and any games that $U_1$ has already rated are removed using
\verb|FILTER|.

Note the use of anonymous nodes (i.e.\ \verb|[]|) in order to de-clutter the
query (and focus the reader's attention on the more relevant variables in play)
and the use of \verb|FILTER NOT EXISTS| as an alternative to the \verb|MINUS|
construction used in the first recommender system.

\subsection{Find information about well-loved designers}\label{sec:rankdesigners}

In order to show off one of the main strengths of RDF and SPARQL over other
data-stores and query languages, a federated query is constructed.\footnote{A
federated query searches multiple data-sources at the same time.}  The federated
query merges information from the BGG data-set with data from DBpedia
\cite{dbpedia}.  Figure~\ref{fig:rq-rankdesigners}
(page~\pageref{fig:rq-rankdesigners}) shows the query: the designers with the
on-average highest scoring games are retrieved from the BGG data-set and
cross-referenced with a descriptive blurb from DBpedia.  The results of the
query are in Table~\ref{tbl:rq-rankdesigners}
(page~\pageref{tbl:rq-rankdesigners}).

Due to technical difficulties,\footnote{The DBpedia SPARQL end-point kept
failing with a 500 HTML error-code.} a relevant sub-set of the DBpedia was
stored locally\footnote{See
\url{http://www.homepages.inf.ed.ac.uk/s0942284/masws/dbpedia-designers.n3}.}
instead of calling the DBpedia SPARQL endpoint repeatedly for every query.  The
subset of interest was downloaded from DBpedia using the \verb|CONSTRUCT| query
in Figure~\ref{fig:rq-dbpedia-designers}.  Unlike the \verb|SELECT| queries used
so far in this report, \verb|CONSTRUCT| queries do not return the results of a
query in triple-form, but rather use the triples to populate a new RDF graph
according to a given pattern.  In essence, this is a bit like creating a view in
a relational database, just more flexible.

Note the use of \verb|LANGMATCHES|, \verb|LANG| and \verb|STR| to select the
DBpedia result in an appropriate language.  Also note the use of \verb|OPTIONAL|
to perform an outer join between \verb|?person| and \verb|?_abstract| or
\verb|?_comment|.  An outer join will return any triples that match
\verb|?person|.  The join will also optionally return any values for
the \verb|?_abstract| and \verb|?_comment| fields, if available.  If
\verb|OPTIONAL| was not used, only \verb|?person|-entities that have a value in
both the \verb|?_abstract| and \verb|?_comment| fields would have been returned.

Besides demonstrating the power of the RDF and SPARQL combination, the query in
Figure~\ref{fig:rq-rankdesigners} is also somewhat revealing.  Four out of the
top ten best reviewed board-game designers in the BGG data-set do not have a
DBpedia entry.  This indicates their niche appeal and thus highlights the fact
that the BGG data-set is a very specialised and expert source of information on
board-games.

\section{The Benefits of RDF+SPARQL}\label{sec:benefits}

The previous section gave a number of sample SPARQL queries to extract
information from the RDF serialisation of the BGG data-set.  The section
identified a number of advantages that the RDF/SPARQL combination offers over
other data-stores and query languages:
\begin{itemize}
\item RDF is semantics-driven where other data-stores (e.g.\ XML or RDBMS) are
more syntax-driven.  This means that querying RDF is simpler (no need to pay
attention to respecting the exact syntactic structure of the data-store) and,
since semantics are universal, queries or schemas are more re-usable.
\item SPARQL is very expressive --- as least as expressive as the
industry-leading query language SQL\@.  At no point during the construction of
the queries in Section~\ref{sec:queries} was any SQL-specific functionality
missed.  Some things were even easier to express in SPARQL than in SQL (e.g.\
easy sub-queries, simple scoped variables, etc.).
\item Perhaps most interestingly, SPARQL makes it trivial to aggregate
information from different data-sources.  Due to strong standardisation, all RDF
databases speak the same language which makes federated queries first-class
citizen in the RDF world.  This allows for an entirely new class of queries to
be considered that would be difficult or tedious to implement with traditional
relational database systems.  For instance, all the queries in
Section~\ref{sec:queries} could be trivially re-written for a relational
database/SQL world --- except for the last federated query which would be
substantially more difficult to translate.
\end{itemize}

However, a couple of disadvantages of using RDF/SPARQL over more mature database
technology also surfaced during the implementation of
Section~\ref{sec:queries}'s queries.  For instance, {\tt arq} was quite slow
compared to the speeds obtainable with relational database management systems
such as DB2, postgreSQL or SQLite.  Additionally, no form of transaction control
was available.  This means that it is risky to use RDF/SPARQL in a more
production-like setting: just one bad query or constructing statement is enough
to potentially leave one's database in a corrupted state, with no
recovery-options.

\section{Conclusion}\label{sec:conclusion}

This report described various ways to extract salient information from the RDF
serialisation of the BoardGameGeek data-set using the SPARQL query language.
(Section~\ref{sec:queries}).  Advantages and disadvantages of using a semantic
data-store and query-language over the traditional relational approach were
discussed in Section~\ref{sec:benefits}.

\clearpage
\include{queries}

\clearpage
\begin{thebibliography}{32}

\bibitem{rdf}
    Lassila, Ora, and Ralph R. Swick,
    ``Resource description framework (RDF) model and syntax specification''
    1999.

\bibitem{sparql}
    Segaran, Toby, Colin Evans, and Jamie Taylor.
    ``Programming the semantic web''
    \emph{Reilly Media, Inc.},
    2009.

\bibitem{egc}
    Epimorphics Ltd.,
    ``Ontology for Games''
    \url{http://epimorphics.com/public/vocabulary/games.ttl},
    retrieved 5 March 2014.

\bibitem{rat}
    Longo, Cristiano and Lorenzo Sciuto,
    ``A Lightweight Ontology for Rating Assessments''
    \url{http://www.tvblob.com/ratings/#},
    \emph{SWAP},
    2007.

\bibitem{foaf}
    Miller, Libby and Brickley, Dan,
    ``Friend of a Friend Ontology''
    \url{http://xmlns.com/foaf/spec/#},
    retrieved 5 March 2014.

\bibitem{jena}
    Apache Software Foundation,
    ``Apache Jena''
    \url{http://jena.apache.org},
    retrieved 7 February 2014.

\bibitem{dbpedia}
    Bizer, Christian, et al.
    ``DBpedia-A crystallisation point for the Web of Data''
    \emph{Web Semantics: Science, Services and Agents on the World Wide Web},
    7.3 (2009): 154-165.

\end{thebibliography}

\end{document}
