#!/bin/bash
set -o errexit
set -o nounset
export PYTHONIOENCODING=utf-8

scriptdir="$(dirname $(readlink -f $0))"
tldir="$(readlink -f $(git rev-parse --show-toplevel))"
data="${tldir}/data/small.xml"
relation_extractor="${tldir}/src/masws/conversion/extract-relation.py"
person_extractor="${tldir}/src/masws/conversion/extract-person.sh"
rating_extractor="${tldir}/src/masws/conversion/extract-rating.py"
dir="$(mktemp -d)"

${relation_extractor} "${data}" --isa \
    ".//boardgame" "" egc BoardGame --isa > "${dir}/BoardGame.n3"
${relation_extractor} "${data}" --isa \
    ".//boardgamepublisher" "" egc Publisher > "${dir}/Publisher.n3"
${relation_extractor} "${data}" \
    ".//boardgamepublisher" "" foaf name > "${dir}/Publisher-name.n3"
${relation_extractor} "${data}" \
    ".//boardgamedesigner" "" foaf name > "${dir}/designer-name.n3"
${relation_extractor} "${data}" \
    ".//boardgame" "./name[@primary=\"true\"]" foaf name > "${dir}/game-name.n3"
${relation_extractor} "${data}" \
    ".//boardgame" ".//boardgamedesigner" egc designed-by > "${dir}/designed-by.n3"
${relation_extractor} "${data}" \
    ".//boardgame" ".//yearpublished" egc pubDate > "${dir}/pubDate.n3"
${relation_extractor} "${data}" --reverse \
    ".//boardgame" ".//boardgamepublisher" egc publisher > "${dir}/publisher.n3"
${person_extractor} "${data}" > "${dir}/Person.n3"
${rating_extractor} "${data}" > "${dir}/Rating.n3"

java -cp "${tldir}/dist/masws.jar" masws.validation.SyntaxCheck "${dir}"/*.n3
cat "${dir}"/*.n3 | sort -r | uniq > bgg.n3

rm -rf "${dir}"
