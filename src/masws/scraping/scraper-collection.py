#!/usr/bin/env python


import errno
import gzip
import itertools
import logging
import os
import re
import sys
import time
import urllib2
import urllib


ROOT = 'http://www.boardgamegeek.com/xmlapi'
API = 'collection'
PARAMS = 'rated=1'
DATA_FORMAT = 'xml'
DATA_FOOTER = '</collections>'
DATA_HEADER = ('<collections termsofuse="'
               'http://boardgamegeek.com/xmlapi/termsofuse">')

DATA_DIR = 'data'
OUT_DIR = os.path.join(DATA_DIR, API)
REQ_USER_AGENT = 'www.inf.ed.ac.uk/teaching/courses/masws'
REQ_DELAY_MULT = 1


def gzip_write(path, data):
    if not path.endswith('.gz'):
        path += '.gz'
    f = gzip.open(path, 'w')
    try:
        f.write(data)
    finally:
        f.close()


def makedirs(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


def delay(seconds):
    logging.info('waiting %d seconds until the next request' % seconds)
    try:
        time.sleep(seconds)
    except KeyboardInterrupt:
        sys.exit(0)


def get(outfile, req_str):
    headers = {'User-Agent': REQ_USER_AGENT}
    request = urllib2.Request(req_str, headers=headers)
    response = urllib2.urlopen(request)
    html = response.read()
    return html


def scrape(items):
    makedirs(OUT_DIR)
    for item in items:
        req_str = '%s/%s/%s?%s' % (ROOT, API, urllib.quote(item), PARAMS)
        outfile = '%s/%s.%s' % (OUT_DIR, item, DATA_FORMAT)

        # perform and time the request
        backoff = 1
        while True:
            try:
                req_time_start = time.time()
                html = get(outfile, req_str)
                req_time_total = max(time.time() - req_time_start, 1)
                break
            except urllib2.HTTPError as e:
                if backoff >= 32:
                    break
                logging.error('request failed with error code %d, backing off'
                              % e.code)
                delay(req_time_total * REQ_DELAY_MULT * backoff)
                backoff *= 2

        # check if we have downloaded anything
        match = re.search('totalitems="(\d+)"', html)
        if match is not None:
            req_result = int(match.groups(1)[0])
            logging.info('scraped %d items from %s' % (req_result, item))
            logging.info('time taken: %d seconds' % req_time_total)
            if req_result > 0:
                gzip_write(outfile, html)

        # be nice: delay the next request
        delay(req_time_total * REQ_DELAY_MULT)


def merge():
    outpath = os.path.join(DATA_DIR, '%s.%s.gz' % (API, DATA_FORMAT))
    outfile = gzip.open(outpath, 'w')
    try:
        outfile.write(DATA_HEADER + '\n')
        paths = os.listdir(OUT_DIR)
        for pathno, path in enumerate(paths):
            logging.info('merged %d/%d' % (pathno + 1, len(paths)))
            datafile = gzip.open(os.path.join(OUT_DIR, path))
            name = path[:-len('.%s.gz' % DATA_FORMAT)]
            try:
                for lineno, line in enumerate(datafile):
                    if lineno == 0:
                        continue
                    elif lineno == 1:
                        outfile.write(re.sub(
                            'termsofuse="[^"]*"', 'user="%s"' % name, line))
                    else:
                        outfile.write(re.sub(
                            '[\000\003\005\010\020\035\037]', '', line))
            finally:
                datafile.close()
        outfile.write(DATA_FOOTER)
    finally:
        outfile.close()


def main():
    import fileinput
    logging.basicConfig(
        level=logging.INFO,
        format='[%(asctime)s,%(process)d:%(levelname)s] %(message)s')
    items = itertools.imap(lambda l: l.strip(), fileinput.input())
    scrape(items)
    merge()


if __name__ == '__main__':
    main()
