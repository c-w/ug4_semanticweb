#!/bin/bash
set -o errexit

# api defines
readonly ROOT='http://www.boardgamegeek.com/xmlapi'
readonly API='boardgame'
readonly PARAMS='comments=1&stats=1'
readonly DATA_FORMAT='xml'
readonly DATA_FOUND='boardgame objectid="[0-9]*"'
readonly DATA_HEADER='<boardgames termsofuse="http://boardgamegeek.com/xmlapi/termsofuse">'
readonly DATA_FOOTER='</boardgames>'


# scraper defines
readonly DATA_DIR="data"
readonly OUT_DIR="${DATA_DIR}/${API}"
readonly REQ_SIZE=100
readonly REQ_DELAY_MULT=1
readonly REQ_USER_AGENT="www.inf.ed.ac.uk/teaching/courses/masws"

# function defines
function log() {
    local date="$(date +%Y-%m-%d\ %H:%M:%S)"
    echo "[${date},$$:INFO] $@" 1>&2
}

function setdefault() {
    [[ -z $1 ]] && echo "$2" || echo "$1"
}

function delay() {
    local T="${1}"
    log "waiting ${T} seconds until the next request"
    sleep "${T}s"
}

function get() {
    local outfile="${1}"
    local request="${2}"
    wget --user-agent="${REQ_USER_AGENT}" -O "${outfile}" "${request}"
}

function main() {
    # parse arguments to script
    req_start="$(setdefault $1 0)"

    # scrape data
    mkdir -p "${OUT_DIR}"
    while true; do
        # build a request
        req_end="$((req_start+${REQ_SIZE}-1))"
        req_items="$(seq --separator=',' ${req_start} ${req_end})"
        req_str="${ROOT}/${API}/${req_items}?${PARAMS}"
        outfile="${OUT_DIR%'/'}/${req_start}-${req_end}.${DATA_FORMAT}"

        # perform and time the request
        backoff="1"
        while true; do
            req_time_start="$(date +%s)"
            get "${outfile}" "${req_str}" && errcode="$?" || errcode="$?"
            req_time_total="$(($(date +%s)-${req_time_start}))"
            if [[ "${errcode}" -eq 0 ]]; then
               break
            else
                log "request failed with error code ${errcode}, backing off"
                delay "$((req_time_total*${REQ_DELAY_MULT}*${backoff}))"
                backoff="$((backoff*2))"
            fi
        done

        # check if we have downloaded everything there is to get
        req_result="$(grep -c -i "${DATA_FOUND}" "${outfile}")"
        log "scraped ${req_result} items in range ${req_start}-${req_end}"
        log "time taken: ${req_time_total} seconds"
        if [[ "${req_result}" -eq 0 ]]; then
            rm "${outfile}"
            log "done"
            break
        fi

        # compress data
        gzip "${outfile}" &

        # be nice: delay the next request
        delay "$((req_time_total*${REQ_DELAY_MULT}))"
        req_start=$((req_start+${REQ_SIZE}))
    done

    log "merging dataset"
    outfile="${DATA_DIR}/${API}.${DATA_FORMAT}"
    {   echo "${DATA_HEADER}"
        zcat "${OUT_DIR}"/*".${DATA_FORMAT}.gz" \
        | egrep -v "${DATA_HEADER}|${DATA_FOOTER}" \
        | tr -d '\000\020'
        echo "${DATA_FOOTER}"
    } > "${outfile}"
    gzip "${outfile}"
    rm -rf "${OUT_DIR}"
}

# run script
main "$@"
