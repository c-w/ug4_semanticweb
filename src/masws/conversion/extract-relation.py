#!/usr/bin/env python2.6


import re
from lxml import etree as ET


ID = 'objectid'
URI = 'http://www.boardgamegeek.com'
ONTOLOGIES = {
    'egc': 'http://elda.googlecode.com/hg/vocabs/games#',
    'foaf': 'http://xmlns.com/foaf/0.1/',
    'a': '',
}


def escape(s, strict=False):
    try:
        float(s)
        return str(s)
    except ValueError:
        pass
    s = '"%s"' % re.sub('"', '\\"', s)
    if strict:
        s = re.sub(' ', '%20', s)
        s = re.sub('\[', '%5B', s)
        s = re.sub('\]', '%5D', s)
    return s


def uri(node):
    try:
        instance = node.get(ID)
    except AttributeError:
        return escape(node)
    if instance is None:
        return escape(node.text)
    instance = escape(instance, strict=True)
    return '<{uri}/{name}/{instance}>'.format(
        uri=URI, name=node.tag, instance=instance)


def findall(parent, child_xpath):
    if not child_xpath:
        return [parent.text]
    if child_xpath.startswith('@'):
        return [parent.attrib[child_xpath[1:]]]
    children = parent.get(child_xpath)
    if children:
        return [children]
    try:
        children = list(parent.iterfind(child_xpath))
        return children
    except StopIteration:
        pass
    raise NotImplementedError()


def format_prefix(rdf_prefix):
    return '@prefix {prefix}:<{ontology}>.'.format(
        prefix=rdf_prefix,
        ontology=ONTOLOGIES[rdf_prefix])


def format_tuple(parent_uri, child_uri, prefix, relation, reverse, isa):
    if isa:
        fmt = (u'{child} a {prefix}:{relation}' if reverse else
               u'{parent} a {prefix}:{relation}')
    else:
        fmt = (u'{child} {prefix}:{relation} {parent}' if reverse else
               u'{parent} {prefix}:{relation} {child}')
    return fmt.format(
        parent=uri(parent),
        child=uri(child),
        prefix=prefix,
        relation=relation) + '.'


if __name__ == '__main__':
    import optparse
    parser = optparse.OptionParser()
    parser.add_option('--reverse', action='store_true', default=False)
    parser.add_option('--isa', action='store_true', default=False)
    opts, args = parser.parse_args()

    tree = ET.parse(args[0])
    parent_xpath = args[1]
    child_xpath = args[2]
    rdf_prefix = args[3]
    rdf_relation = args[4]
    reverse = opts.reverse
    isa = opts.isa

    print format_prefix(rdf_prefix)
    for parent in tree.iterfind(parent_xpath):
        for child in findall(parent, child_xpath):
            args = parent, child, rdf_prefix, rdf_relation, reverse, isa
            print format_tuple(*args)
