#!/usr/bin/env python2.6


import re
from lxml import etree as ET
BGG = u'<http://www.boardgamegeek.com>'
NUMS = set(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'))


def uri(value, api):
    value = re.sub(' ', '%20', value)
    value = re.sub('\[', '%5B', value)
    value = re.sub('\]', '%5D', value)
    return u'<http://www.boardgamegeek.com/%s/%s>' % (api, value)


if __name__ == '__main__':
    import optparse
    parser = optparse.OptionParser()
    opts, args = parser.parse_args()

    tree = ET.parse(args[0])
    user_uri = lambda p: uri(p, 'user')
    boardgame_uri = lambda p: uri(p, 'boardgame')

    print u'@prefix rat:<http://www.tvblob.com/ratings/#>.'
    print u'{collector} a rat:RatingsCollector.'.format(collector=BGG)
    print u'{collector} rat:mode rat:EXPLICIT_MODE.'.format(collector=BGG)
    print u'{collector} rat:hasRatingValuesLowerBound 1.'.format(collector=BGG)
    print u'{collector} rat:hasRatingValuesUpperBound 10.'.format(collector=BGG)
    i = 0
    for boardgame in tree.iterfind('.//boardgame'):
        boardgameid = boardgame.attrib['objectid']
        for comment in boardgame.iterfind('.//comment'):
            user = comment.attrib['username']
            rating = comment.attrib['rating']
            if rating not in NUMS:
                continue
            print u'_:p{anon} rat:assessedBy {user}.'.format(
                anon=i, user=user_uri(user))
            print u'_:p{anon} rat:about {boardgame}.'.format(
                anon=i, boardgame=boardgame_uri(boardgameid))
            print u'_:p{anon} rat:value {rating}.'.format(
                anon=i, rating=rating)
            print u'_:p{anon} rat:collectedBy {collector}.'.format(
                anon=i, collector=BGG)
            i += 1
