#!/bin/bash

data="$(readlink -f $1)"

echo "@prefix foaf:<http://xmlns.com/foaf/0.1/>."
less "$data" \
| grep -o 'username="[^"]*' \
| sed 's,username=",,' \
| sed 's, ,%20,g' \
| sed 's,\[,%5B,g' \
| sed 's,\],%5D,g' \
| sed 's,^,<http://www.boardgamegeek.com/user/,' \
| sed 's,$,> a foaf:Person.,'
