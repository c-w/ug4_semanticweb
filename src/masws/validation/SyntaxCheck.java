package masws.validation;

import com.hp.hpl.jena.util.FileManager;


public class SyntaxCheck {
    public static void main(String[] args) {
        for (String dataUrl : args) {
	        try {
	            FileManager.get().loadModel(dataUrl, getExtension(dataUrl));
	            System.err.println("[Validation OK] " + dataUrl);
	        } catch (Exception e) {
	            System.err.println("[Validation ERROR] " + dataUrl + "(" + e.getMessage() + ")");
	        }
        }
    }

    private static String getExtension(String fileName) {
        String[] tokens = fileName.split("\\.(?=[^\\.]+$)");
        return tokens[1];
    }
}
