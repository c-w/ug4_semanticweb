#!/bin/bash
# Fetches additional packages into the directory of the script
# Adds the packages to .gitignore to avoid cluttering the repository
set -o errexit


# utility declarations
scriptdir="$(dirname $(readlink -f $0))"

function echo_emph() {
    echo -e "\e[44m$1\e[49m"
}

function add_to_gitignore() {
    path="$(basename $1)"
    gitignore="$scriptdir/.gitignore"
    ([[ -f "$gitignore" ]] && grep -q "$path" "$gitignore") || echo "$path" >> $gitignore
}


# Apache Jena
function get_apache_jena() {
    local data_url='http://mirror.catn.com/pub/apache//jena/binaries/apache-jena-2.11.1.zip'
    local outpath="$scriptdir/$(basename $data_url .zip)"
    [[ -d $outpath ]] && return
    echo_emph "Apache Jena"
    local tmpfile=$(mktemp)
    wget -O $tmpfile $data_url
    unzip $tmpfile -d $scriptdir
    add_to_gitignore $outpath
    rm -f $tmpfile
    echo "done"
}


# parse arguments
[[ -z $@ ]] && echo "please specify package to fetch" && exit 1
for arg in "$@"; do
    case $arg in
        jena) get_apache_jena ;;
        *) echo "unknown package $arg"
    esac
done
